import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';

/*Navigation routes*/
//import * as routes from './PageRoutes/routes';

/*Compnent and Pages*/
import AdminNavbar from './Components/utils/NavigationBar'

import HomePage from './Pages/HomePage'

const AdminApp = () =>
  <Router>
    <div>
    <AdminNavbar/>
    <div class="content-wrapper">
    <HomePage/>
    </div>
    </div>
  </Router>

export default AdminApp
