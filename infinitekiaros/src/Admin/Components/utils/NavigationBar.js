import React from 'react';
import {Input, Fa, Navbar, NavbarBrand, NavbarNav, NavbarToggler, Collapse,
  NavItem, NavLink, Dropdown, DropdownToggle, DropdownMenu,  Container,Footer }
  from 'mdbreact';
  import 'bootstrap/dist/css/bootstrap.min.css';
  import 'mdbreact/docs/css/mdb.css';
  import 'font-awesome/css/font-awesome.min.css';
  import '../../../styles/css/style.css';

  import 'bootstrap/dist/js/bootstrap.min.js';
const AdminNavbar = () =>
  <div>
      <NavigationAuth/>
  </div>

  class NavigationAuth extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                collapse: false,
                isWideEnough: false,
                dropdownOpen: false,
            };
        this.onClick = this.onClick.bind(this);
        this.toggle = this.toggle.bind(this);

        }

        onClick(){
            this.setState({
                collapse: !this.state.collapse,
            });
        }

        toggle() {
            this.setState({
                dropdownOpen: !this.state.dropdownOpen
            });
        }

        render() {

            return (
              <nav className="navbar fixed-top navbar-expand-lg navbar-light white" id="mainNav">

                <NavLink className="navbar-brand waves-effect" to="#!">
                <strong className="blue-text">Infinit-Kiaro</strong> </NavLink>

                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon black black "></span>
                </button>


                <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav  navbar-sidenav sidebar-fixed" id="exampleAccordion">

                  <li className="nav-item list-group list-group-flush" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a className="nav-link   waves-effect" href="index.html">
                      <i className="fa  fa-dashboard mr-3"></i>
                      <span className="nav-link-text">Dashboard</span>
                    </a>
                  </li>

                  <li className=" nav-item list-group list-group-flush" data-toggle="tooltip" data-placement="right" title="Users">
                    <a className="nav-link" href="tables.html">
                      <i className="fa fa-user mr-3"></i>
                      <span className="nav-link-text">Manage Users</span>
                    </a>
                  </li>

                  <li className="nav-item list-group list-group-flush " data-toggle="tooltip" data-placement="right" title="Content">
                    <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseContent" >
                      <i className="fa fa-fw fa-wrench mr-3"></i>
                      <span className="nav-link-text">Manage Content</span>
                    </a>
                    <ul className="sidenav-second-level collapse" id="collapseContent">
                      <li>
                        <a href="#!">Photography</a>
                      </li>
                      <li>
                        <a href="cards.html">Fitness</a>
                      </li>
                    </ul>
                  </li>

                  <li className="nav-item  list-group list-group-flush" data-toggle="tooltip" data-placement="right" title="Packages">
                    <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapsePackages" >
                      <i className="fa fa-fw fa-wrench mr-3"></i>
                      <span className="nav-link-text">Manage Packages</span>
                    </a>
                    <ul className="sidenav-second-level collapse" id="collapsePackages">
                      <li>
                        <a href="#!">Photography</a>
                      </li>
                      <li>
                        <a href="cards.html">Fitness</a>
                      </li>
                    </ul>
                  </li>

                  <li className="nav-item  list-group list-group-flush" data-toggle="tooltip" data-placement="right" title="Users">
                    <a className="nav-link " href="tables.html">
                      <i className="fa fa-fw fa-table mr-3"></i>
                      <span className="nav-link-text">Manage Website</span>
                    </a>
                  </li>

                  </ul>
                  <ul className="navbar-nav sidebar-fixed sidenav-toggler">
                    <li className="nav-item">
                      <a className="nav-link text-center" id="sidenavToggler">
                        <i className="fa fa-fw fa-angle-left"></i>
                      </a>
                    </li>
                  </ul>
                  <ul className="navbar-nav ml-auto">
                  <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-fw fa-envelope"></i>
                  <span className="d-lg-none">Messages
                  <span className="badge badge-pill badge-primary">12 New</span>
                  </span>
                  <span className="indicator text-primary d-none d-lg-block">
                  <i className="fa fa-fw fa-circle"></i>
                  </span>
                  </a>
                  <div className="dropdown-menu" aria-labelledby="messagesDropdown">
                  <h6 className="dropdown-header">New Messages:</h6>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">
                  <strong>David Miller</strong>
                  <span className="small float-right text-muted">11:21 AM</span>
                  <div className="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they dont overflow over to the sides!</div>
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">
                  <strong>Jane Smith</strong>
                  <span className="small float-right text-muted">11:21 AM</span>
                  <div className="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">
                  <strong>John Doe</strong>
                  <span className="small float-right text-muted">11:21 AM</span>
                  <div className="dropdown-message small">Ive sent the final files over to you for review. When youre able to sign off of them let me know and we can discuss distribution.</div>
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item small" href="#">View all messages</a>
                  </div>
                  </li>
                  <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-fw fa-bell"></i>
                  <span className="d-lg-none">Alerts
                  <span className="badge badge-pill badge-warning">6 New</span>
                  </span>
                  <span className="indicator text-warning d-none d-lg-block">
                  <i className="fa fa-fw fa-circle"></i>
                  </span>
                  </a>
                  <div className="dropdown-menu" aria-labelledby="alertsDropdown">
                  <h6 className="dropdown-header">New Alerts:</h6>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">
                  <span className="text-success">
                  <strong>
                    <i className="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
                  </span>
                  <span className="small float-right text-muted">11:21 AM</span>
                  <div className="dropdown-message small">This is an automated server response message. All systems are online.</div>
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">
                  <span className="text-danger">
                  <strong>
                    <i className="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>
                  </span>
                  <span className="small float-right text-muted">11:21 AM</span>
                  <div className="dropdown-message small">This is an automated server response message. All systems are online.</div>
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">
                  <span className="text-success">
                  <strong>
                    <i className="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
                  </span>
                  <span className="small float-right text-muted">11:21 AM</span>
                  <div className="dropdown-message small">This is an automated server response message. All systems are online.</div>
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item small" href="#">View all alerts</a>
                  </div>
                  </li>
                  <li className="nav-item">
                  <a className="nav-link" data-toggle="modal" data-target="#exampleModal">
                  <i className="fa fa-fw fa-sign-out"></i>Logout</a>
                  </li>
              </ul>
                </div>
          </nav>
            );
        }
    }

const navContent = () =>


class NavigationNoAuth extends React.Component {
        constructor(props) {
            super(props);

        }

        render() {

            return (
              <div>
              </div>

            );
        }
    }



export default AdminNavbar;
