import React from 'react';
import {BrowserRouter as Router,Route} from 'react-router-dom';

/*Navigation routes*/
import * as routes from '../Constants/routes';

//css
import './App.css';

import AdminApp from '../Admin/AdminApp';

const App = () =>
  <Router>
    <div>
    <Route exact path={routes.ADMINPAGE} component={() => <AdminApp />} />
    </div>
  </Router>

export default App
